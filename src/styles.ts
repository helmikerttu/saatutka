import styled from 'styled-components';

export const Box = styled.div`
  background-color: #ffffff;
  border-radius: 4px;
  border: 1px solid #e6e6e6;
  padding: 6px;
  margin-bottom: 12px;
`;
